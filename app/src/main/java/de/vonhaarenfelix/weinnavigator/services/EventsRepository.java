package de.vonhaarenfelix.weinnavigator.services;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.List;

import de.vonhaarenfelix.weinnavigator.models.Company;
import de.vonhaarenfelix.weinnavigator.models.Event;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The type Events repository.
 */
@SuppressWarnings("NullableProblems")
public class EventsRepository {
    private static EventsRepository eventsRepository;

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static EventsRepository getInstance() {
        if (eventsRepository == null) {
            eventsRepository = new EventsRepository();
        }
        return eventsRepository;
    }

    private final ApiService apiService;

    private EventsRepository() {
        apiService = RetrofitService.createService(ApiService.class);
    }


    /**
     * Gets events.
     *
     * @return the events
     */
    public MutableLiveData<List<Event>> getEvents() {
        MutableLiveData<List<Event>> eventsData = new MutableLiveData<>();
        apiService.getEvents().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                if (response.isSuccessful()) {
                    eventsData.setValue(response.body());
                }else{
                    Log.e("API ERROR", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.e("API ERROR", t.getMessage());


            }
        });
        return eventsData;
    }

    /**
     * Gets event.
     *
     * @param id the id
     * @return the event
     */
    public MutableLiveData<Event> getEvent(String id) {
        MutableLiveData<Event> eventData = new MutableLiveData<>();
        apiService.getEvent(id).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                if (response.isSuccessful()) {
                    eventData.setValue(response.body());
                }else{
                    Log.e("API ERROR", response.message());
                }
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                Log.e("API ERROR", t.getMessage());
                eventData.setValue(null);
            }
        });
        return eventData;
    }

    /**
     * Gets company.
     *
     * @param id the id
     * @return the company
     */
    public MutableLiveData<Company> getCompany(String id) {
        MutableLiveData<Company> companyData = new MutableLiveData<>();
        apiService.getCompany(id).enqueue(new Callback<Company>() {
            @Override
            public void onResponse(Call<Company> call, Response<Company> response) {
                if (response.isSuccessful()) {
                    companyData.setValue(response.body());
                }else{
                    Log.e("API ERROR", response.message());
                }
            }

            @Override
            public void onFailure(Call<Company> call, Throwable t) {
                Log.e("API ERROR", t.getMessage());
                companyData.setValue(null);
            }
        });
        return companyData;
    }
}
