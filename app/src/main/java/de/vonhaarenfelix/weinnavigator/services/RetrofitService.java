package de.vonhaarenfelix.weinnavigator.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.vonhaarenfelix.weinnavigator.BuildConfig;
import de.vonhaarenfelix.weinnavigator.models.Band;
import de.vonhaarenfelix.weinnavigator.models.Company;
import de.vonhaarenfelix.weinnavigator.models.Organizer;
import de.vonhaarenfelix.weinnavigator.models.Winegrower;
import de.vonhaarenfelix.weinnavigator.utils.RuntimeTypeAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The type Retrofit service.
 */
class RetrofitService {
    private static Gson createCompanyAdapter(){
        RuntimeTypeAdapterFactory<Company> runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory
                .of(Company.class, "itemType")
                .registerSubtype(Organizer.class, "Organizer")
                .registerSubtype(Winegrower.class, "Winegrower")
                .registerSubtype(Band.class, "Band");

        return new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapterFactory(runtimeTypeAdapterFactory)
                .create();
    }

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create(createCompanyAdapter()))
            .build();

    /**
     * Create service s.
     *
     * @param <S>          the type parameter
     * @param serviceClass the service class
     * @return the s
     */
    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
