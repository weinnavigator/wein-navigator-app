package de.vonhaarenfelix.weinnavigator.services;

import java.util.List;

import de.vonhaarenfelix.weinnavigator.models.Company;
import de.vonhaarenfelix.weinnavigator.models.Event;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * The interface Api service.
 */
public interface ApiService {
    /**
     * Gets events.
     *
     * @return the events
     */
    @GET("events")
    Call<List<Event>> getEvents();

    /**
     * Gets event.
     *
     * @param eventId the event id
     * @return the event
     */
    @GET("events/{eventId}")
    Call<Event> getEvent(@Path("eventId") String eventId);

    /**
     * Gets company.
     *
     * @param companyId the company id
     * @return the company
     */
    @GET("companies/{companyId}")
    Call<Company> getCompany(@Path("companyId") String companyId);
}
