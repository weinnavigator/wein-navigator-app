package de.vonhaarenfelix.weinnavigator.models.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import de.vonhaarenfelix.weinnavigator.models.Event;
import de.vonhaarenfelix.weinnavigator.services.EventsRepository;

/**
 * The type Event view model.
 */
public class EventViewModel extends ViewModel {
    private MutableLiveData<Event> mutableLiveData;

    /**
     * Init.
     *
     * @param id the id
     */
    public void init(String id) {
        if (mutableLiveData != null) {
            return;
        }
        EventsRepository eventsRepository = EventsRepository.getInstance();
        mutableLiveData = eventsRepository.getEvent(id);
    }

    /**
     * Gets event.
     *
     * @return the event
     */
    public LiveData<Event> getEvent() {
        return mutableLiveData;
    }
}
