package de.vonhaarenfelix.weinnavigator.models.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import de.vonhaarenfelix.weinnavigator.models.Event;
import de.vonhaarenfelix.weinnavigator.services.EventsRepository;


/**
 * The type Events view model.
 */
public class EventsViewModel extends ViewModel {

    private MutableLiveData<List<Event>> mutableLiveData;

    /**
     * Init.
     */
    public void init() {
        EventsRepository eventsRepository = EventsRepository.getInstance();
        mutableLiveData = eventsRepository.getEvents();
    }

    /**
     * Refresh.
     */
    public void refresh() {
        EventsRepository eventsRepository = EventsRepository.getInstance();
        mutableLiveData = eventsRepository.getEvents();
    }

    /**
     * Gets events.
     *
     * @return the events
     */
    public LiveData<List<Event>> getEvents() {
        return mutableLiveData;
    }
}
