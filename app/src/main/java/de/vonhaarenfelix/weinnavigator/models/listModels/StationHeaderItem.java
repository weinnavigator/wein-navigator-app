package de.vonhaarenfelix.weinnavigator.models.listModels;

/**
 * The type Station header item.
 */
@SuppressWarnings("SameReturnValue")
public class StationHeaderItem extends ListItem {

    private int number;
    private boolean toilet;

    /**
     * Sets number.
     *
     * @param number the number
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * Get name string.
     *
     * @return the string
     */
    public String getName(){
        return "Station "+number;
    }

    /**
     * Is toilet boolean.
     *
     * @return the boolean
     */
    public boolean isToilet() {
        return toilet;
    }

    /**
     * Sets toilet.
     *
     * @param toilet the toilet
     */
    public void setToilet(boolean toilet) {
        this.toilet = toilet;
    }

    @Override
    public int getType() {
        return TYPE_HEADER;
    }
}
