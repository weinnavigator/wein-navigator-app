package de.vonhaarenfelix.weinnavigator.models;

import org.json.JSONException;
import org.json.JSONObject;

import static de.vonhaarenfelix.weinnavigator.utils.Utils.getFloat;
import static de.vonhaarenfelix.weinnavigator.utils.Utils.getString;

/**
 * The type Weather data.
 */
@SuppressWarnings("ALL")
public class WeatherData {
    /**
     * The Temp min.
     */
    final int temp_min;
    /**
     * The Temp max.
     */
    final int temp_max;
    /**
     * The Current weather desc.
     */
    final String currentWeatherDesc;
    /**
     * The Icon.
     */
    final String icon;
    /**
     * The Time.
     */
    final int time;

    // Konstruktor für Klasse WeatherData für den Forecast
    private WeatherData(int temp_min, int temp_max, String currentWeatherDesc, String icon, int time) {
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.currentWeatherDesc = currentWeatherDesc;
        this.icon = icon;
        this.time = time;
    }

    /**
     * Parse weather object weather data.
     *
     * @param rawWeather the raw weather
     * @return the weather data
     */
    static WeatherData parseWeatherObject(JSONObject rawWeather) {
        try {
            JSONObject tempObj = rawWeather.getJSONObject("temp");
            JSONObject weatherObj = rawWeather.getJSONArray("weather").getJSONObject(0);

            //int day = (int) Math.round(getFloat(tempObj, "day"));
            int temp_min = (int) Math.round(getFloat(tempObj, "min"));
            int temp_max = (int) Math.round(getFloat(tempObj, "max"));
            //int rain = (int) Math.round(getFloat(rawWeather, "rain"));
            //String currentWeather = getString(weatherObj, "main");
            String currentWeatherDesc = getString(weatherObj, "description");
            String icon = getString(weatherObj, "icon");
            int time = rawWeather.getInt("dt");

            return new WeatherData(temp_min, temp_max, currentWeatherDesc, icon, time);
        } catch (JSONException e) {
            return null;
        }
    }
}
