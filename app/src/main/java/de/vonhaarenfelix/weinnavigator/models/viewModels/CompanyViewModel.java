package de.vonhaarenfelix.weinnavigator.models.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import de.vonhaarenfelix.weinnavigator.models.Company;
import de.vonhaarenfelix.weinnavigator.services.EventsRepository;

/**
 * The type Company view model.
 */
public class CompanyViewModel extends ViewModel {
    private MutableLiveData<Company> mutableLiveData;

    /**
     * Init.
     *
     * @param id the id
     */
    public void init(String id) {
        if (mutableLiveData != null) {
            return;
        }
        EventsRepository eventsRepository = EventsRepository.getInstance();
        mutableLiveData = eventsRepository.getCompany(id);
    }

    /**
     * Gets company.
     *
     * @return the company
     */
    public LiveData<Company> getCompany() {
        return mutableLiveData;
    }
}
