package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Food.
 */
public class Food {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Double number;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets number.
     *
     * @return the number
     */
    public Double getNumber() {
        return number;
    }

}
