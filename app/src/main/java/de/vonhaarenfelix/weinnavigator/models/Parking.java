package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Parking.
 */
public class Parking {
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("parkType")
    @Expose
    private String parkType;

    /**
     * Gets lat.
     *
     * @return the lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * Gets lon.
     *
     * @return the lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * Gets park type.
     *
     * @return the park type
     */
    public String getParkType() {
        return parkType;
    }

    /**
     * Sets park type.
     *
     * @param parkType the park type
     */
    public void setParkType(String parkType) {
        this.parkType = parkType;
    }
}
