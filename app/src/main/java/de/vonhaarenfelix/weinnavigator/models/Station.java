package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The type Station.
 */
public class Station {
    @SerializedName("companies")
    @Expose
    private final List<Company> companies = null;
    @SerializedName("toilet")
    @Expose
    private Boolean toilet;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;
    @SerializedName("sortIndex")
    @Expose
    private Integer sortIndex;

    /**
     * Gets companies.
     *
     * @return the companies
     */
    public List<Company> getCompanies() {
        //noinspection ConstantConditions
        return companies;
    }


    /**
     * Gets toilet.
     *
     * @return the toilet
     */
    public Boolean getToilet() {
        return toilet;
    }

    /**
     * Gets lat.
     *
     * @return the lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * Gets lon.
     *
     * @return the lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * Gets sort index.
     *
     * @return the sort index
     */
    public Integer getSortIndex() {
        return sortIndex;
    }

}
