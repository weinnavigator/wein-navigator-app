package de.vonhaarenfelix.weinnavigator.models.listModels;

import de.vonhaarenfelix.weinnavigator.models.Performance;

/**
 * The type Performance item.
 */
public class PerformanceItem extends ListItem {

    private Performance performance;

    /**
     * Gets performance.
     *
     * @return the performance
     */
    public Performance getPerformance() {
        return performance;
    }

    /**
     * Sets performance.
     *
     * @param performance the performance
     */
    public void setPerformance(Performance performance) {
        this.performance = performance;
    }

    @Override
    public int getType() {
        return TYPE_BODY;
    }
}
