package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The type Winegrower.
 */
public class Winegrower extends Company {
    @SerializedName("food")
    @Expose
    private final List<Food> food;
    @SerializedName("drinks")
    @Expose
    private final List<Drink> drinks;

    /**
     * Instantiates a new Winegrower.
     *
     * @param food   the food
     * @param drinks the drinks
     */
    public Winegrower(List<Food> food, List<Drink> drinks) {
        this.food = food;
        this.drinks = drinks;
    }

    /**
     * Gets food.
     *
     * @return the food
     */
    public List<Food> getFood() {
        return food;
    }

    /**
     * Gets drinks.
     *
     * @return the drinks
     */
    public List<Drink> getDrinks() {
        return drinks;
    }


    @Override
    public String getCategory() {
        return "Weingut";
    }
}
