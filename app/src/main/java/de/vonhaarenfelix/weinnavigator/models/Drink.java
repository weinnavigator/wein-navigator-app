package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The type Drink.
 */
public class Drink {
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("prices")
    @Expose
    private final List<DrinkPrice> prices = null;
    @SerializedName("alcohol")
    @Expose
    private Double alcohol;

    /**
     * Gets year.
     *
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Gets desc.
     *
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Gets alcohol.
     *
     * @return the alcohol
     */
    public Double getAlcohol() {
        return alcohol;
    }

    /**
     * Gets prices.
     *
     * @return the prices
     */
    public List<DrinkPrice> getPrices() {
        //noinspection ConstantConditions
        return prices;
    }

}
