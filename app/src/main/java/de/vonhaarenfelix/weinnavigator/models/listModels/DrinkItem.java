package de.vonhaarenfelix.weinnavigator.models.listModels;

import de.vonhaarenfelix.weinnavigator.models.Drink;

/**
 * The type Drink item.
 */
public class DrinkItem extends ListItem {
    private Drink drink;

    /**
     * Gets drink.
     *
     * @return the drink
     */
    public Drink getDrink() {
        return drink;
    }

    /**
     * Sets drink.
     *
     * @param drink the drink
     */
    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    @Override
    public int getType() {
        return TYPE_BODY;
    }
}
