package de.vonhaarenfelix.weinnavigator.models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import de.vonhaarenfelix.weinnavigator.R;

/**
 * The type Company.
 */
public abstract class Company {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("instagram")
    @Expose
    private String instagram;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("zip")
    @Expose
    private Integer zip;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("itemType")
    @Expose
    private String itemType;
    @SerializedName("banner")
    @Expose
    private String banner;

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    private String getTel() {
        return tel;
    }

    private String getEmail() {
        return email;
    }

    private String getWebsite() {
        return website;
    }

    private String getFacebook() {
        return facebook;
    }

    private String getTwitter() {
        return twitter;
    }

    private String getInstagram() {
        return instagram;
    }

    private String getStreet() {
        return street;
    }

    private Integer getZip() {
        return zip;
    }

    private String getCity() {
        return city;
    }

    /**
     * Gets logo.
     *
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * Gets banner.
     *
     * @return the banner
     */
    public String getBanner() {
        return banner;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public abstract String getCategory();

    /**
     * Create company view.
     *
     * @param activity   the activity
     * @param context    the context
     * @param bodyLayout the body layout
     */
    public void createCompanyView(Activity activity, Context context, LinearLayout bodyLayout) {
        TextView categoryTextView = bodyLayout.findViewById(R.id.organizer_category);
        categoryTextView.setText(getCategory());
        TextView nameTextView = bodyLayout.findViewById(R.id.organizer_name);
        nameTextView.setText(getName());
        ImageView logoImageView = bodyLayout.findViewById(R.id.organizer_logo);
        setImage(logoImageView, getLogo());

        LinearLayout socialWrapper = bodyLayout.findViewById(R.id.event_socials);
        //Socials
        if (getFacebook() != null) {
            socialWrapper.addView(createImageButton(activity, context, R.drawable.ic_facebook, getFacebook()));
        }

        if (getTwitter() != null) {
            socialWrapper.addView(createImageButton(activity, context, R.drawable.ic_twitter, getTwitter()));
        }

        if (getInstagram() != null) {
            socialWrapper.addView(createImageButton(activity, context, R.drawable.ic_instagram, getInstagram()));
        }

        //zip and city
        if (getZip() != null && getCity() != null) {
            LinearLayout.LayoutParams layoutParamsZip = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParamsZip.setMargins(20, 0, 20, 40);
            bodyLayout.addView(createTextView(context, getZip() + " " + getCity(), layoutParamsZip),1);
        }

        //street
        if (getStreet() != null) {
            LinearLayout.LayoutParams layoutParamsStreet = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParamsStreet.setMargins(20, 20, 20, 5);
            bodyLayout.addView(createTextView(context, getStreet(), layoutParamsStreet),1);
        }


        //Contact
        if (getTel() != null) {
            bodyLayout.addView(createContactField(context, R.drawable.ic_phone_white_24dp, getTel()), 1);
        }
        if (getEmail() != null) {
            bodyLayout.addView(createContactField(context, R.drawable.ic_mail_white_24dp, getEmail()), 1);
        }
        if (getWebsite() != null) {
            bodyLayout.addView(createContactField(context, R.drawable.ic_link_white_24dp, getWebsite()), 1);
        }
    }

    private LinearLayout createContactField(Context context, int iconId, String text) {
        LinearLayout parent = new LinearLayout(context);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 20, 20);
        parent.setLayoutParams(layoutParams);
        parent.setOrientation(LinearLayout.HORIZONTAL);
        int dpUnit = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, context.getResources().getDisplayMetrics());
        ImageView imageView = new ImageView(context);
        LinearLayout.LayoutParams imageViewLayoutParams = new LinearLayout.LayoutParams(dpUnit, dpUnit);
        imageViewLayoutParams.setMarginEnd(20);
        imageView.setLayoutParams(imageViewLayoutParams);
        imageView.setForegroundGravity(Gravity.CENTER_VERTICAL);
        imageView.setImageResource(iconId);

        parent.addView(imageView);
        parent.addView(createTextView(context, text));

        return parent;
    }

    private TextView createTextView(Context context, String text) {
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setTextSize(14);
        textView.setTextColor(context.getResources().getColor(R.color.colorAccent, context.getTheme()));
        textView.setTypeface(ResourcesCompat.getFont(context, R.font.neutra_text_book));
        textView.setText(text);

        return textView;
    }

    private TextView createTextView(Context context, String text, LinearLayout.LayoutParams params) {
        TextView textView = createTextView(context, text);
        textView.setLayoutParams(params);

        return textView;
    }


    private ImageButton createImageButton(Activity activity, Context context, int iconId, String url) {
        ImageButton button = new ImageButton(context);
        button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        button.setImageResource(iconId);

        button.setOnClickListener((View v) -> {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            activity.startActivity(i);
        });
        return button;
    }

    private void setImage(ImageView view, String url) {
        Picasso.get()
                .load(url)
                .transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        int targetWidth = view.getWidth();
                        double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                        int targetHeight = (int) (targetWidth * aspectRatio);
                        if (targetHeight <= 0 || targetWidth <= 0) return source;
                        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                        if (result != source) {
                            // Same bitmap is returned if sizes are the same
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "key";
                    }
                })
                .into(view);
    }

}