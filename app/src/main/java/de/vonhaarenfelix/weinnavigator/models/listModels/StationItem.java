package de.vonhaarenfelix.weinnavigator.models.listModels;

import de.vonhaarenfelix.weinnavigator.models.Company;

/**
 * The type Station item.
 */
@SuppressWarnings("SameReturnValue")
public class StationItem extends ListItem {
    private Company company;

    /**
     * Gets company.
     *
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Sets company.
     *
     * @param company the company
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public int getType() {
        return TYPE_BODY;
    }
}
