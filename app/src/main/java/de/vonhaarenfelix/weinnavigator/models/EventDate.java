package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * The type Event date.
 */
public class EventDate {

    private static final String dateFormat = "dd.MM.YY";
    private static final String timeFormat = "HH:mm";

    @SerializedName("start")
    @Expose
    private Integer start;
    @SerializedName("end")
    @Expose
    private Integer end;

    /**
     * Gets start.
     *
     * @return the start
     */
    public Integer getStart() {
        return start;
    }

    /**
     * Gets end.
     *
     * @return the end
     */
    public Integer getEnd() {
        return end;
    }

    /**
     * Gets dates.
     *
     * @param sep the sep
     * @return the dates
     */
    public String getDates(String sep) {
        if (sameDay()) {
            return this.toLocal(getStart(), dateFormat);
        }
        return this.toLocal(getStart(), dateFormat) + sep + this.toLocal(getEnd(), dateFormat);
    }

    /**
     * Gets times.
     *
     * @param sep the sep
     * @return the times
     */
    public String getTimes(String sep) {
        return this.toLocal(getStart(), timeFormat) + sep + this.toLocal(getEnd(), timeFormat);
    }

    private String toLocal(long timestamp, String dateFormat) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
        sdf.setTimeZone(tz);
        return sdf.format(new Date(timestamp * 1000));
    }

    /**
     * Same day boolean.
     *
     * @return the boolean
     */
    public boolean sameDay() {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        fmt.setTimeZone(tz); // your time zone
        return fmt.format(new Date(getStart() * 1000)).equals(fmt.format(new Date(getEnd() * 1000)));
    }

    /**
     * Is in range of forecast boolean.
     *
     * @return the boolean
     */
    public Boolean isInRangeOfForecast() {
        Date start = new Date((long) getStart() * 1000);
        Date end = new Date((long) getEnd() * 1000);

        Calendar startCal = Calendar.getInstance();
        startCal.setTime(start);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(end);

        Calendar nowCal = Calendar.getInstance();
        Calendar now6Cal = Calendar.getInstance();
        now6Cal.add(Calendar.DATE, 6);

        //Check if start is in range of the forecast
        return (startCal.get(Calendar.DAY_OF_YEAR) >= nowCal.get(Calendar.DAY_OF_YEAR) && startCal.get(Calendar.YEAR) == nowCal.get(Calendar.YEAR) &&
                startCal.get(Calendar.DAY_OF_YEAR) <= now6Cal.get(Calendar.DAY_OF_YEAR) && startCal.get(Calendar.YEAR) == now6Cal.get(Calendar.YEAR));
    }
}
