package de.vonhaarenfelix.weinnavigator.models;

/**
 * The type Organizer.
 */
public class Organizer extends Company {
    //We could extend the organizer class here, but this polymorphism is only for working with the runtimeTypeAdapterFactory

    @Override
    public String getCategory() {
        return "Veranstalter";
    }
}
