package de.vonhaarenfelix.weinnavigator.models;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Event location.
 */
public class EventLocation {
    @SerializedName("zip")
    @Expose
    private Integer zip;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lon")
    @Expose
    private Double lon;

    private String distance;

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Gets lat.
     *
     * @return the lat
     */
    public Double getLat() {
        return lat;
    }

    private Double getLon() {
        return lon;
    }

    /**
     * Gets distance.
     *
     * @return the distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * Sets distance.
     *
     * @param currentLocation the current location
     */
    public void setDistance(Location currentLocation) {
        Location eventLocation = new Location("Event Location");
        eventLocation.setLatitude(getLat());
        eventLocation.setLongitude(getLon());
        distance = Math.round(currentLocation.distanceTo(eventLocation) / 1000)+" km";
    }

    /**
     * Sets distance.
     *
     * @param distance the distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }
}
