package de.vonhaarenfelix.weinnavigator.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.vonhaarenfelix.weinnavigator.BuildConfig;
import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.interfaces.AsyncResponse;
import de.vonhaarenfelix.weinnavigator.interfaces.OnWeatherUpdate;
import de.vonhaarenfelix.weinnavigator.network.ApiRequest;
import de.vonhaarenfelix.weinnavigator.utils.UrlBuilder;

import static java.util.Arrays.asList;

/**
 * The type Weather.
 */
@SuppressWarnings("ALL")
public class Weather {
    private final ArrayList<WeatherData> weatherData = new ArrayList<>();
    private final EventDate date;
    private final Double lat;
    private final Double lon;
    private final OnWeatherUpdate mListener;


    /**
     * Instantiates a new Weather.
     *
     * @param onWeatherUpdate the on weather update
     * @param lat             the lat
     * @param lon             the lon
     * @param date            the date
     */
    public Weather(OnWeatherUpdate onWeatherUpdate, double lat, double lon, EventDate date) {
        this.lat = lat;
        this.lon = lon;
        this.date = date;
        this.mListener = onWeatherUpdate;
        getWeather();
    }

    private static int getResId(String resName) {
        try {
            Field idField = R.drawable.class.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return R.drawable.ic_noweather;
        }
    }

    private WeatherResult initWeather(EventDate date, JSONObject rawWeather) {
        parseWeather(rawWeather);
        ArrayList<WeatherData> weather = new ArrayList<>();
        // Wenn das Event nur an einem Tag stattfindet (am selben Tag endet wie beginnt)
        if (date.sameDay()) {
            WeatherData wData = getWeatherByDay(date.getStart());
            if (wData != null) {
                weather.add(wData);
            }
            return parseWeatherToWeatherResult(weather);
        }
        // Wenn das Event über mehrere Tage geht
        else {
            for (int i = 0; i < calcEventRange(); i++) {
                Date curDate = new Date((long) date.getStart() * 1000);
                Calendar curCal = Calendar.getInstance();
                curCal.setTime(curDate);
                curCal.add(Calendar.DATE, i);
                WeatherData wData = getWeatherByDay(curCal.getTimeInMillis() / 1000);
                if (wData != null) {
                    weather.add(wData);
                }
            }
        }

        return parseWeatherToWeatherResult(weather);
    }

    private int calcEventRange() {
        Date start = new Date((long) date.getStart() * 1000);
        Date end = new Date((long) date.getEnd() * 1000);

        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();

        startCal.setTime(start);
        endCal.setTime(end);

        return endCal.get(Calendar.DAY_OF_YEAR) - startCal.get(Calendar.DAY_OF_YEAR);
    }

    private WeatherResult parseWeatherToWeatherResult(ArrayList<WeatherData> weatherList) {
        if (weatherList.size() == 1) {
            WeatherData weatherData = weatherList.get(0);
            return new WeatherResult(weatherData.temp_min, weatherData.temp_max, getResId("ic_" + weatherData.icon), weatherData.currentWeatherDesc);
        } else if (weatherList.size() != 0) {
            WeatherResult weatherResult = new WeatherResult(weatherList.get(0).temp_min, weatherList.get(0).temp_max, getResId("ic_" + weatherList.get(0).icon), weatherList.get(0).currentWeatherDesc);
            for (WeatherData weather : weatherList) {
                weatherResult.setMin(weather.temp_min);
                weatherResult.setMax(weather.temp_max);
            }
            return weatherResult;
        }

        return new WeatherResult(0, 0, R.drawable.ic_noweather, "Keine Daten");
    }

    private WeatherData getWeatherByDay(long date) {
        for (WeatherData forecastDay : weatherData) {
            Date forecastDate = new Date((long) forecastDay.time * 1000);
            Calendar cal = Calendar.getInstance();
            cal.setTime(forecastDate);

            Date eventDate = new Date(date * 1000);
            Calendar eventCal = Calendar.getInstance();
            eventCal.setTime(eventDate);
            if (cal.get(Calendar.DAY_OF_YEAR) == eventCal.get(Calendar.DAY_OF_YEAR)) {
                return forecastDay;
            }
        }

        return null;
    }

    private void parseWeather(JSONObject rawWeather) {
        try {
            JSONArray list = rawWeather.getJSONArray("list");
            for (int i = 0; i < list.length(); i++) {
                weatherData.add(WeatherData.parseWeatherObject(list.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //WeatherLocation weatherLocation = WeatherLocation.parseForecastWeatherLocation(rawWeather);
    }

    private void getWeather() {
        AsyncResponse asyncResp = (JSONObject output) -> mListener.update(initWeather(date, output));
        if (date.isInRangeOfForecast()) {
            ApiRequest asyncTask = new ApiRequest(asyncResp);

            Map<String, String> paramMap = new HashMap<>();
            paramMap.put("APPID", BuildConfig.WEATHER_API_KEY);
            paramMap.put("units", "metric");
            paramMap.put("lat", Double.toString(lat));
            paramMap.put("lon", Double.toString(lon));
            paramMap.put("lang", "de");

            asyncTask.execute(UrlBuilder.buildUri(
                    BuildConfig.WEATHER_API_URL,
                    asList("forecast", "daily"),
                    paramMap
            ));
        } else {
            mListener.update(new WeatherResult(0, 0, R.drawable.ic_noweather, "Keine Daten"));
        }
    }

    /**
     * The type Weather result.
     */
    public class WeatherResult {
        /**
         * The Icon.
         */
        final int icon;
        /**
         * The Desc.
         */
        final String desc;
        /**
         * The Min.
         */
        int min;
        /**
         * The Max.
         */
        int max;

        private WeatherResult(int min, int max, int icon, String desc) {
            this.min = min;
            this.max = max;
            this.icon = icon;
            this.desc = desc;
        }

        /**
         * Gets min.
         *
         * @return the min
         */
        public String getMin() {
            return min + "°";
        }

        private void setMin(int temp) {
            if (this.min > temp) {
                this.min = temp;
            }
        }

        /**
         * Gets max.
         *
         * @return the max
         */
        public String getMax() {
            return max + "°";
        }

        private void setMax(int temp) {
            if (this.max < temp) {
                this.max = temp;
            }
        }

        /**
         * Gets icon.
         *
         * @return the icon
         */
        public int getIcon() {
            return icon;
        }

        /**
         * Gets desc.
         *
         * @return the desc
         */
        public String getDesc() {
            return desc;
        }
    }
}
