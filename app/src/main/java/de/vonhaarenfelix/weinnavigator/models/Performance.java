package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Performance.
 */
public class Performance {
    @SerializedName("stationName")
    @Expose
    private String stationName;
    @SerializedName("eventDate")
    @Expose
    private EventDate eventDate;

    /**
     * Instantiates a new Performance.
     *
     * @param stationName the station name
     */
    public Performance(String stationName) {
        this.stationName = stationName;
    }

    /**
     * Gets station name.
     *
     * @return the station name
     */
    public String getStationName() {
        return stationName;
    }

    /**
     * Sets station name.
     *
     * @param stationName the station name
     */
    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    /**
     * Gets event date.
     *
     * @return the event date
     */
    public EventDate getEventDate() {
        return eventDate;
    }

    /**
     * Sets event date.
     *
     * @param eventDate the event date
     */
    public void setEventDate(EventDate eventDate) {
        this.eventDate = eventDate;
    }
}
