package de.vonhaarenfelix.weinnavigator.models;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The type Event.
 */
public class Event implements Comparable<Event> {

    @SerializedName("eventDate")
    @Expose
    private EventDate eventDate;
    @SerializedName("eventLocation")
    @Expose
    private EventLocation eventLocation;
    @SerializedName("parking")
    @Expose
    private final List<Parking> parking = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("previewUrl")
    @Expose
    private String previewUrl;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("organizer")
    @Expose
    private Company organizer;
    @SerializedName("stations")
    @Expose
    private final List<Station> stations = null;

    /**
     * Gets event date.
     *
     * @return the event date
     */
    public EventDate getEventDate() {
        return eventDate;
    }

    /**
     * Gets event location.
     *
     * @return the event location
     */
    public EventLocation getEventLocation() {
        return eventLocation;
    }

    /**
     * Gets parking.
     *
     * @return the parking
     */
    public List<Parking> getParking() {
        //noinspection ConstantConditions
        return parking;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets preview url.
     *
     * @return the preview url
     */
    public String getPreviewUrl() {
        return previewUrl;
    }

    /**
     * Gets image url.
     *
     * @return the image url
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets organizer.
     *
     * @return the organizer
     */
    public Company getOrganizer() {
        return organizer;
    }

    /**
     * Gets stations.
     *
     * @return the stations
     */
    public List<Station> getStations() {
        //noinspection ConstantConditions
        return stations;
    }

    @NonNull
    @Override
    public String toString() {
        return getId() + "|" + getName();
    }

    @Override
    public int compareTo(Event ev) {
        return Long.compare(this.eventDate.getStart(), ev.eventDate.getEnd());
    }
}
