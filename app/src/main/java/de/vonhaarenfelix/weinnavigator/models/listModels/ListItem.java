package de.vonhaarenfelix.weinnavigator.models.listModels;

/**
 * The type List item.
 */
public abstract class ListItem {

    /**
     * The constant TYPE_HEADER.
     */
    public static final int TYPE_HEADER = 0;
    /**
     * The constant TYPE_BODY.
     */
    public static final int TYPE_BODY = 1;

    /**
     * Gets type.
     *
     * @return the type
     */
    abstract public int getType();
}
