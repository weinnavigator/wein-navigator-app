package de.vonhaarenfelix.weinnavigator.models.listModels;

/**
 * The type Header item.
 */
public class HeaderItem extends ListItem {

    private String headline;

    /**
     * Gets headline.
     *
     * @return the headline
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * Sets headline.
     *
     * @param headline the headline
     */
    public void setHeadline(String headline) {
        this.headline = headline;
    }

    @Override
    public int getType() {
        return TYPE_HEADER;
    }
}
