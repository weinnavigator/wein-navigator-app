package de.vonhaarenfelix.weinnavigator.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The type Band.
 */
public class Band extends Company {
    @SerializedName("performances")
    @Expose
    private final List<Performance> performances;

    /**
     * Instantiates a new Band.
     *
     * @param performances the performances
     */
    public Band(List<Performance> performances) {
        this.performances = performances;
    }

    /**
     * Gets performances.
     *
     * @return the performances
     */
    public List<Performance> getPerformances() {
        return performances;
    }


    @Override
    public String getCategory() {
        return "Band";
    }
}
