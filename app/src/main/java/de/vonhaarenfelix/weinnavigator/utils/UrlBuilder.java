package de.vonhaarenfelix.weinnavigator.utils;

import android.net.Uri;

import java.util.List;
import java.util.Map;

/**
 * The type Url builder.
 */
public class UrlBuilder {


    /**
     * Build uri uri.
     *
     * @param baseUrl the base url
     * @param paths   the paths
     * @param params  the params
     * @return the uri
     */
    public static Uri buildUri(String baseUrl, List<String> paths, Map<String, String> params) {
        Uri.Builder builder = Uri.parse(baseUrl).buildUpon();

        addPaths(builder, paths);
        addParams(builder, params);
        return builder.build();
    }

    private static void addPaths(Uri.Builder builder, List<String> paths) {
        for (int i = 0; i < paths.size(); i++) {
            builder.appendPath(paths.get(i));
        }
    }

    private static void addParams(Uri.Builder builder, Map<String, String> params) {
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }
    }
}
