package de.vonhaarenfelix.weinnavigator.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The type Utils.
 */
public class Utils {
    /**
     * Gets float.
     *
     * @param obj the obj
     * @param key the key
     * @return the float
     */
    public static double getFloat(JSONObject obj, String key) {
        try {
            return obj.getDouble(key);
        } catch (JSONException e) {
            return 0.0;
        }
    }

    /**
     * Gets string.
     *
     * @param obj the obj
     * @param key the key
     * @return the string
     */
    public static String getString(JSONObject obj, String key) {
        try {
            return obj.getString(key);
        } catch (JSONException e) {
            return "";
        }
    }
}
