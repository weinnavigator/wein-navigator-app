package de.vonhaarenfelix.weinnavigator.network;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.interfaces.AsyncResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * The type Api request.
 */
public class ApiRequest extends AsyncTask<Uri, Void, JSONObject> {
    private final OkHttpClient client = new OkHttpClient();

    //Link Interface
    private final AsyncResponse delegate;

    /**
     * Instantiates a new Api request.
     *
     * @param delegate the delegate
     */
    public ApiRequest(AsyncResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected JSONObject doInBackground(Uri... uri) {
        Uri singleUri = uri[0];
        return run(singleUri);
    }

    private JSONObject run(Uri url) {
        Request request = new Request.Builder()
                .url(url.toString())
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                String jsonString = Objects.requireNonNull(response.body()).string();
                return new JSONObject(jsonString);
            } else {
                Log.e("events", Integer.toString(response.code()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        super.onPostExecute(result);
        delegate.processFinish(result);
    }
}


