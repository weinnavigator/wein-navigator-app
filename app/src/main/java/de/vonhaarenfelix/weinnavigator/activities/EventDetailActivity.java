package de.vonhaarenfelix.weinnavigator.activities;

import android.animation.ValueAnimator;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.adapters.EventPagerAdapter;
import de.vonhaarenfelix.weinnavigator.fragments.FragmentEventInfo;
import de.vonhaarenfelix.weinnavigator.fragments.FragmentEventStations;
import de.vonhaarenfelix.weinnavigator.interfaces.FragmentCommunication;
import de.vonhaarenfelix.weinnavigator.models.viewModels.EventViewModel;

/**
 * The type Event detail activity.
 */
public class EventDetailActivity extends AppCompatActivity implements FragmentCommunication {
    private TextView mEventTitleTextView;
    private ImageView mEventImageView;
    private TextView mEventCategoryTextView;

    private RelativeLayout eventTopWrapper;

    private FragmentEventInfo fragmentEventInfo;

    private static void expand(final View v, int targetHeight) {
        int prevHeight = v.getHeight();
        v.setVisibility(View.VISIBLE);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener((ValueAnimator animation) -> {
            v.getLayoutParams().height = (int) animation.getAnimatedValue();
            v.requestLayout();
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(200);
        valueAnimator.start();
    }

    private static void collapse(final View v) {
        int prevHeight = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, 0);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener((ValueAnimator animation) -> {
            v.getLayoutParams().height = (int) animation.getAnimatedValue();
            v.requestLayout();
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(200);
        valueAnimator.start();
    }

    private void setImage(String url) {
        Picasso.get()
                .load(url)
                .centerCrop()
                .fit()
                .into(mEventImageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("Image", e.toString());
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    public void onRouteInformation(String routeKm, String routeMin) {
        if (fragmentEventInfo != null) {
            fragmentEventInfo.updateRoute(routeKm, routeMin);
        } else {
            Log.e("route", "No Fragment found");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        ViewPager eventDetailViewPager = findViewById(R.id.eventDetailViewPager);
        mEventTitleTextView = findViewById(R.id.eventTitle);
        mEventImageView = findViewById(R.id.eventImage);
        mEventCategoryTextView = findViewById(R.id.eventCategory);
        eventTopWrapper = findViewById(R.id.eventTopWrapper);

        // Toolbar

        // Set ViewPageAdapter and TabLayout
        ArrayList<Fragment> listFragments = new ArrayList<>();
        fragmentEventInfo = new FragmentEventInfo();
        FragmentEventStations fragmentEventStations = new FragmentEventStations();
        listFragments.add(fragmentEventInfo);
        listFragments.add(fragmentEventStations);
        fragmentEventStations.setOnHeadlineSelectedListener(this);
        EventPagerAdapter eventDetailViewPagerAdapter = new EventPagerAdapter(getSupportFragmentManager(), listFragments);
        eventDetailViewPager.setAdapter(eventDetailViewPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.eventDetailTabLayout);
        tabLayout.setupWithViewPager(eventDetailViewPager);

        //Model Setup
        EventViewModel eventViewModel = ViewModelProviders.of(this).get(EventViewModel.class);
        eventViewModel.init(getIntent().getStringExtra("_id"));
        eventViewModel.getEvent().observe(this, eventResponse -> {
            mEventTitleTextView.setText(Objects.requireNonNull(eventResponse).getName());
            setImage(eventResponse.getImageUrl());
            mEventCategoryTextView.setText(eventResponse.getCategory());
            eventResponse.getEventLocation().setDistance(getIntent().getStringExtra("distance"));
        });

        ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int pos) {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                float logicalDensity = metrics.density;
                int px = (int) Math.ceil(300 * logicalDensity);

                if (pos == 1) {
                    collapse(eventTopWrapper);
                } else {
                    expand(eventTopWrapper, px);
                }
            }
        };
        eventDetailViewPager.addOnPageChangeListener(mPageChangeListener);
    }
}