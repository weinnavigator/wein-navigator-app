package de.vonhaarenfelix.weinnavigator.activities;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.adapters.EventsAdapter;
import de.vonhaarenfelix.weinnavigator.models.Event;
import de.vonhaarenfelix.weinnavigator.models.viewModels.EventsViewModel;
import de.vonhaarenfelix.weinnavigator.services.LocationService;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;


/**
 * The type Main activity.
 *
 * @author Felix von Haaren
 */
public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private static final String[] PERMS = {Manifest.permission.ACCESS_FINE_LOCATION};

    private final ArrayList<Event> eventsArrayList = new ArrayList<>();

    private ProgressBar mEventsProgressBar;
    private TextView mLoadStatusTextView;
    private EventsAdapter mAdapter;
    private RecyclerView mEventsRecyclerView;
    private ImageView mRefreshEventButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mEventsRecyclerView = findViewById(R.id.recycler_view_events);
        mEventsProgressBar = findViewById(R.id.progress_bar_events);
        mLoadStatusTextView = findViewById(R.id.text_view_load_status);
        mRefreshEventButton = findViewById(R.id.refreshEvents);
        requestLocationPermission();

        Toolbar mActionBarToolbar = findViewById(R.id.event_list_action_bar);
        setSupportActionBar(mActionBarToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.toolbar_event_title);
        mActionBarToolbar.setTitleTextColor(Color.WHITE);

        mRefreshEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadEventsData();
            }
        });

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mEventsRecyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(mEventsRecyclerView.getContext().getResources().getDrawable(R.drawable.event_list_divider, null));
        mEventsRecyclerView.addItemDecoration(dividerItemDecoration);

        loadEventsData();
        setupRecyclerView();
    }

    private void loadEventsData() {
        EventsViewModel eventsViewModel = ViewModelProviders.of(this).get(EventsViewModel.class);
        eventsArrayList.clear();
        eventsViewModel.init();
        eventsViewModel.getEvents().observe(this, events -> {
            eventsArrayList.addAll(Objects.requireNonNull(events));
            if (EasyPermissions.hasPermissions(this, PERMS)) {
                calculateDistances();
            }
            mAdapter.notifyDataSetChanged();
            showData();
        });
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new EventsAdapter(eventsArrayList, (Event event) -> {
                Intent detailEventIntent = new Intent(mEventsRecyclerView.getContext(), EventDetailActivity.class);
                detailEventIntent.putExtra("_id", event.getId());
                detailEventIntent.putExtra("distance", event.getEventLocation().getDistance());
                startActivity(detailEventIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            });

            mEventsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mEventsRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void showData() {
        mLoadStatusTextView.setVisibility(View.GONE);
        mEventsProgressBar.setVisibility(View.GONE);
    }

    private void calculateDistances() {
        LocationService.getLocationManager(getApplicationContext(), (Location location) -> {
            for (int i = 0; i < eventsArrayList.size(); i++) {
                eventsArrayList.get(i).getEventLocation().setDistance(location);
            }
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> list) {
        calculateDistances();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> list) {
        // Nothing to do here
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(1)
    private void requestLocationPermission() {
        if (!EasyPermissions.hasPermissions(this, PERMS)) {
            EasyPermissions.requestPermissions(
                    new PermissionRequest.Builder(this, 1, PERMS).build());
        }
    }
}
