package de.vonhaarenfelix.weinnavigator.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.adapters.BandPagerAdapter;
import de.vonhaarenfelix.weinnavigator.adapters.WinegrowerPagerAdapter;
import de.vonhaarenfelix.weinnavigator.models.Band;
import de.vonhaarenfelix.weinnavigator.models.Winegrower;
import de.vonhaarenfelix.weinnavigator.models.viewModels.CompanyViewModel;

/**
 * The type Company activity.
 */
public class CompanyActivity extends AppCompatActivity {

    private ViewPager eventCompanyViewPager;
    private TextView companyCategoryTextView;
    private TextView companyNameTextView;
    private ImageView companyLogoImageView;
    private ImageView companyBannerImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_company);
        //UI
        companyCategoryTextView = findViewById(R.id.company_category);
        companyNameTextView = findViewById(R.id.company_name);
        companyLogoImageView = findViewById(R.id.company_logo);
        companyBannerImageView = findViewById(R.id.company_banner);

        CompanyViewModel companyViewModel = ViewModelProviders.of(this).get(CompanyViewModel.class);
        companyViewModel.init(getIntent().getStringExtra("_id"));
        companyViewModel.getCompany().observe(this, company -> {
            companyCategoryTextView.setText(Objects.requireNonNull(company).getCategory());
            companyNameTextView.setText(company.getName());
            setImage(companyLogoImageView, company.getLogo());
            setImageFullscreen(companyBannerImageView, company.getBanner());

            if (company instanceof Winegrower) {
                setupWinegrower();
            } else if (company instanceof Band) {
                setupBand();
            } else {
                Log.e("Company Error", "Unknown company");
            }
        });
    }

    private void setupWinegrower() {
        eventCompanyViewPager = findViewById(R.id.company_view_pager);
        // Set ViewPageAdapter and TabLayout
        WinegrowerPagerAdapter eventCompanyPagerAdapter = new WinegrowerPagerAdapter(getSupportFragmentManager());
        eventCompanyViewPager.setAdapter(eventCompanyPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.company_tab_layout);
        tabLayout.setupWithViewPager(eventCompanyViewPager);
    }

    private void setupBand() {
        eventCompanyViewPager = findViewById(R.id.company_view_pager);
        // Set ViewPageAdapter and TabLayout
        BandPagerAdapter eventCompanyPagerAdapter = new BandPagerAdapter(getSupportFragmentManager());
        eventCompanyViewPager.setAdapter(eventCompanyPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.company_tab_layout);
        tabLayout.setupWithViewPager(eventCompanyViewPager);
    }

    private void setImageFullscreen(ImageView view, String url) {
        if (!url.equals("")) {
            Picasso.get()
                    .load(url)
                    .centerCrop()
                    .fit()
                    .into(view, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("Image", e.toString());
                        }
                    });
        }
    }

    private void setImage(ImageView view, String url) {
        if (!url.equals("")) {
            Picasso.get()
                    .load(url)
                    .into(view, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("Image", e.toString());
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
