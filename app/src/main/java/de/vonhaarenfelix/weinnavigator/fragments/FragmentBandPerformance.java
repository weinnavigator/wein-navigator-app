package de.vonhaarenfelix.weinnavigator.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.adapters.PerformanceAdapter;
import de.vonhaarenfelix.weinnavigator.models.Band;
import de.vonhaarenfelix.weinnavigator.models.Performance;
import de.vonhaarenfelix.weinnavigator.models.viewModels.CompanyViewModel;

/**
 * The type Fragment band performance.
 */
public class FragmentBandPerformance extends Fragment {
    private final ArrayList<Performance> performances = new ArrayList<>();
    private RecyclerView recyclerView;
    private PerformanceAdapter mAdapter;

    /**
     * Instantiates a new Fragment band performance.
     */
    public FragmentBandPerformance() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sorted_recycler, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycler_view);

        CompanyViewModel companyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CompanyViewModel.class);
        companyViewModel.getCompany().observe(this, company -> {
            Band band = (Band) company;
            performances.addAll(Objects.requireNonNull(band).getPerformances());
            setupRecyclerView();
        });
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new PerformanceAdapter(performances);

            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }
}
