package de.vonhaarenfelix.weinnavigator.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.adapters.DrinkAdapter;
import de.vonhaarenfelix.weinnavigator.models.Drink;
import de.vonhaarenfelix.weinnavigator.models.Winegrower;
import de.vonhaarenfelix.weinnavigator.models.viewModels.CompanyViewModel;


/**
 * The type Fragment winegrower drinks.
 */
public class FragmentWinegrowerDrinks extends Fragment {
    private final ArrayList<Drink> drinks = new ArrayList<>();
    private RecyclerView mDrinksRecyclerView;
    private DrinkAdapter mAdapter;

    /**
     * Instantiates a new Fragment winegrower drinks.
     */
    public FragmentWinegrowerDrinks() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sorted_recycler, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Recycler View Setup
        mDrinksRecyclerView = view.findViewById(R.id.recycler_view);

        CompanyViewModel companyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CompanyViewModel.class);
        companyViewModel.getCompany().observe(this, company -> {
            Winegrower winegrower = (Winegrower) company;
            drinks.addAll(Objects.requireNonNull(winegrower).getDrinks());
            setupRecyclerView();
        });
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new DrinkAdapter(drinks);

            mDrinksRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            mDrinksRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }
}

