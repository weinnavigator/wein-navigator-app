package de.vonhaarenfelix.weinnavigator.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.adapters.FoodAdapter;
import de.vonhaarenfelix.weinnavigator.models.Winegrower;
import de.vonhaarenfelix.weinnavigator.models.viewModels.CompanyViewModel;


/**
 * The type Fragment winegrower food.
 */
public class FragmentWinegrowerFood extends ListFragment {
    /**
     * Instantiates a new Fragment winegrower food.
     */
    public FragmentWinegrowerFood() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_winegrower_food, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CompanyViewModel companyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CompanyViewModel.class);
        companyViewModel.getCompany().observe(this, company -> {
            Winegrower winegrower = (Winegrower) company;
            setListAdapter(new FoodAdapter(getContext(), Objects.requireNonNull(winegrower).getFood()));
        });
    }
}
