package de.vonhaarenfelix.weinnavigator.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.models.viewModels.CompanyViewModel;


/**
 * The type Fragment company info.
 */
public class FragmentCompanyInfo extends Fragment {

    private LinearLayout company_info_lL;

    /**
     * Instantiates a new Fragment company info.
     */
    public FragmentCompanyInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        company_info_lL = Objects.requireNonNull(getView()).findViewById(R.id.event_kontakt_wrapper);
        if (getActivity() != null) {
            CompanyViewModel companyViewModel = ViewModelProviders.of(getActivity()).get(CompanyViewModel.class);
            companyViewModel.getCompany().observe(this, company -> Objects.requireNonNull(company).createCompanyView(getActivity(), getContext(), company_info_lL));
        }
    }
}
