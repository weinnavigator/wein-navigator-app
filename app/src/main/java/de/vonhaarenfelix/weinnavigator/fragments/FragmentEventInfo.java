package de.vonhaarenfelix.weinnavigator.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.interfaces.FragmentCommunication;
import de.vonhaarenfelix.weinnavigator.models.Event;
import de.vonhaarenfelix.weinnavigator.models.Weather;
import de.vonhaarenfelix.weinnavigator.models.viewModels.EventViewModel;

/**
 * The type Fragment event info.
 */
public class FragmentEventInfo extends Fragment {

    private TextView mEventDescriptionTextView;
    private TextView mEventRouteDistanceTextView;
    private TextView mEventRouteMinutesTextView;
    private TextView mEventWeatherTempTextView;
    private TextView mEventWeatherStatusTextView;
    private ImageView mEventWeatherIconImageView;
    private Boolean descriptionExpanded = false;
    private FragmentCommunication mCallback;
    private String routeKm;
    private String routeMin = "Calculating ...";

    /**
     * Instantiates a new Fragment event info.
     */
    public FragmentEventInfo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (FragmentCommunication) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement DataCommunication");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_info, container, false);
    }

    /**
     * Update route.
     *
     * @param km  the km
     * @param min the min
     */
    public void updateRoute(String km, String min) {
        routeKm = km;
        routeMin = min;
        if (mEventRouteDistanceTextView != null) {
            mEventRouteDistanceTextView.setText(km);
        }

        if (mEventRouteMinutesTextView != null) {
            mEventRouteMinutesTextView.setText(min);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            EventViewModel eventViewModel = ViewModelProviders.of(getActivity()).get(EventViewModel.class);
            eventViewModel.getEvent().observe(this, event -> {
                View mainView = getView();
                if (mainView != null) {
                    setupTabs(mainView, event);
                    //Setup Organizer
                    setupOrganizer(mainView, Objects.requireNonNull(event));
                }
                new Weather((weatherResult) -> {
                    String tempDesc = weatherResult.getMin() + " - " + weatherResult.getMax();
                    mEventWeatherTempTextView.setText(tempDesc);
                    mEventWeatherStatusTextView.setText(weatherResult.getDesc());
                    mEventWeatherIconImageView.setImageResource(weatherResult.getIcon());
                }, Objects.requireNonNull(event).getEventLocation().getLat(), event.getEventLocation().getLat(), event.getEventDate());
            });
        }
    }

    private void setupTabs(View mainView, Event event) {
        mEventDescriptionTextView = mainView.findViewById(R.id.eventDescription);

        //Event Date
        View eventDateWrapper = mainView.findViewById(R.id.event_date_wrapper);
        TextView mEventDateTextView = eventDateWrapper.findViewById(R.id.topText);
        TextView mEventTimeTextView = eventDateWrapper.findViewById(R.id.bottomText);

        //Route distance
        View routeDistanceWrapper = mainView.findViewById(R.id.event_route_distance_wrapper);
        mEventRouteDistanceTextView = routeDistanceWrapper.findViewById(R.id.topText);
        mEventRouteMinutesTextView = routeDistanceWrapper.findViewById(R.id.bottomText);
        if (routeKm != null) {
            mEventRouteDistanceTextView.setText(routeKm);
        }

        if (routeMin != null) {
            mEventRouteMinutesTextView.setText(routeMin);
        }
        //Weather
        View weatherWrapper = mainView.findViewById(R.id.event_weather_wrapper);
        mEventWeatherTempTextView = weatherWrapper.findViewById(R.id.topText);
        mEventWeatherStatusTextView = weatherWrapper.findViewById(R.id.bottomText);
        mEventWeatherIconImageView = weatherWrapper.findViewById(R.id.icon);

        //Stations
        View stationsWrapper = mainView.findViewById(R.id.event_stations_wrapper);
        TextView mEventStationsCountTextView = stationsWrapper.findViewById(R.id.text);
        mEventStationsCountTextView.setText(String.format(Locale.getDefault(), "%d", event.getStations().size()));
        //Location
        View locationWrapper = mainView.findViewById(R.id.event_location_wrapper);
        TextView mEventLocationCityTextView = locationWrapper.findViewById(R.id.topText);
        TextView mEventLocationDistanceTextView = locationWrapper.findViewById(R.id.bottomText);
        mEventLocationCityTextView.setText(event.getEventLocation().getCity());
        mEventLocationDistanceTextView.setText(event.getEventLocation().getDistance());
        //Icons
        ImageView alarmIconImageView = eventDateWrapper.findViewById(R.id.icon);
        alarmIconImageView.setImageResource(R.drawable.ic_alarm_white_24dp);

        ImageView routeDistanceIconImageView = routeDistanceWrapper.findViewById(R.id.icon);
        routeDistanceIconImageView.setImageResource(R.drawable.ic_refresh_white_24dp);

        ImageView stationsIconImageView = stationsWrapper.findViewById(R.id.icon);
        stationsIconImageView.setImageResource(R.drawable.ic_store_mall_directory_white_24dp);

        ImageView locationIconImageView = locationWrapper.findViewById(R.id.icon);
        locationIconImageView.setImageResource(R.drawable.ic_location_on_white_24dp);

        //Event Description
        String description = event.getDescription();
        description = description.substring(0, 190) + "...";
        mEventDescriptionTextView.setText(description);

        mEventDescriptionTextView.setOnClickListener((View v) -> {
            if (descriptionExpanded) {
                String desc = event.getDescription().substring(0, 190) + "...";
                mEventDescriptionTextView.setText(desc);
                descriptionExpanded = false;
            } else {
                mEventDescriptionTextView.setText(event.getDescription());
                descriptionExpanded = true;
            }
        });

        //Event Date
        mEventDateTextView.setText(event.getEventDate().getDates(" - "));
        mEventTimeTextView.setText(event.getEventDate().getTimes(" - "));
    }

    private void setupOrganizer(View mainView, Event event) {
        LinearLayout event_company_layout = mainView.findViewById(R.id.event_kontakt_wrapper);

        event.getOrganizer().createCompanyView(getActivity(), getContext(), event_company_layout);
    }


}
