package de.vonhaarenfelix.weinnavigator.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.NetworkLocationIgnorer;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.mylocation.DirectedLocationOverlay;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.BuildConfig;
import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.activities.CompanyActivity;
import de.vonhaarenfelix.weinnavigator.adapters.StationAdapter;
import de.vonhaarenfelix.weinnavigator.interfaces.FragmentCommunication;
import de.vonhaarenfelix.weinnavigator.models.Company;
import de.vonhaarenfelix.weinnavigator.models.Parking;
import de.vonhaarenfelix.weinnavigator.models.Station;
import de.vonhaarenfelix.weinnavigator.models.viewModels.EventViewModel;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;


/**
 * The type Fragment event stations.
 */
@SuppressWarnings({"unchecked", "EmptyMethod", "SameReturnValue"})
public class FragmentEventStations extends Fragment implements LocationListener, MapEventsReceiver {
    private final ArrayList<Station> cardStations = new ArrayList<>();
    private final ArrayList<Station> stations = new ArrayList<>();
    private final NetworkLocationIgnorer mIgnorer = new NetworkLocationIgnorer();
    /**
     * The M road.
     */
    protected Road mRoad;
    private MapView map = null;
    private DirectedLocationOverlay myLocationOverlay;
    private LocationManager mLocationManager;
    private FolderOverlay mRoadNodeMarkers;
    private Polyline mRoadOverlays;
    private StationAdapter mAdapter;
    private RecyclerView mStationsRecyclerView;
    private BottomSheetBehavior bottomSheetBehavior;
    private FragmentCommunication callback;
    private long mLastTime = 0;

    private static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * Sets on headline selected listener.
     *
     * @param callback the callback
     */
    public void setOnHeadlineSelectedListener(FragmentCommunication callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
        Context ctx = Objects.requireNonNull(getActivity()).getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);

        myLocationOverlay = new DirectedLocationOverlay(ctx);
        myLocationOverlay.setDirectionArrow(drawableToBitmap(getResources().getDrawable(R.drawable.ic_navigation_black_24dp, null)));

        mRoadNodeMarkers = new FolderOverlay();
        mRoadNodeMarkers.setName("Route Steps");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            callback = (FragmentCommunication) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement DataCommunication");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_stations, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    private boolean startLocationUpdates() {
        boolean result = false;
        for (final String provider : mLocationManager.getProviders(true)) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationManager.requestLocationUpdates(provider, 2 * 1000, 0.0f, this);
                result = true;
            }
        }
        return result;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        map = view.findViewById(R.id.map);
        mStationsRecyclerView = view.findViewById(R.id.recycler_view_stations);
        //Bottom Sheet
        LinearLayout mBottomSheet = view.findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(mBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        //Map Setup
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setTilesScaledToDpi(true);
        map.setMultiTouchControls(true);
        map.setMinZoomLevel(1.0);
        map.setMaxZoomLevel(20.0);
        map.setVerticalMapRepetitionEnabled(false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mStationsRecyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(mStationsRecyclerView.getContext().getResources().getDrawable(R.drawable.event_list_divider, null));
        mStationsRecyclerView.addItemDecoration(dividerItemDecoration);

        MapEventsOverlay mapEventsOverlay = new MapEventsOverlay(this);
        map.getOverlays().add(0, mapEventsOverlay);

        map.getOverlays().add(myLocationOverlay);

        if (savedInstanceState == null) {
            Location location = null;
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location == null)
                    location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (location != null) {
                onLocationChanged(location);
            } else {
                myLocationOverlay.setEnabled(false);
            }
        } else {
            myLocationOverlay.setLocation(savedInstanceState.getParcelable("location"));
        }
        setupRecyclerView();
        map.getOverlays().add(1, mRoadNodeMarkers);
        EventViewModel eventViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(EventViewModel.class);
        setView();
        eventViewModel.getEvent().observe(this, event -> {
            List<Parking> parking = Objects.requireNonNull(event).getParking();

            //Add spots
            for (int i = 0; i < parking.size(); i++) {
                Parking park = parking.get(i);
                Marker parkMarker = new Marker(map);
                if (park.getParkType().equals("car")) {
                    Drawable icon = getResources().getDrawable(R.drawable.ic_local_parking_black_24dp, null);
                    parkMarker.setIcon(icon);
                    parkMarker.setTitle("Parkplatz");
                } else {
                    parkMarker.setIcon(getResources().getDrawable(R.drawable.ic_directions_bus_black_24dp, null));
                    parkMarker.setTitle("Bushaltestelle");
                }
                parkMarker.setPosition(new GeoPoint(park.getLat(), park.getLon()));
                parkMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER);

                map.getOverlays().add(parkMarker);
                putRoadNodes(event.getStations());

                //Setup init all Stations
                cardStations.addAll(event.getStations());
                stations.clear();
                stations.addAll(event.getStations());
                setupRecyclerView();
                setView();
                getRoadAsync(event.getStations());
            }
        });
        map.invalidate();
    }

    private void putRoadNodes(List<Station> stations) {
        mRoadNodeMarkers.getItems().clear();
        Drawable icon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_location_on_red_24dp, null);
        int n = stations.size();
        for (int i = 0; i < n; i++) {
            Station station = stations.get(i);
            Marker nodeMarker = new Marker(map);
            nodeMarker.setPosition(new GeoPoint(station.getLat(), station.getLon()));
            nodeMarker.setOnMarkerClickListener((Marker marker, MapView mapView) -> {
                onStationClick(station);
                return true;
            });
            nodeMarker.setIcon(icon);
            nodeMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            mRoadNodeMarkers.add(nodeMarker);
        }
        map.invalidate();
    }

    private void setupRecyclerView() {
        if (mAdapter == null) {
            mAdapter = new StationAdapter(cardStations, (Company company) -> {
                Intent companyIntent = new Intent(getActivity(), CompanyActivity.class);
                companyIntent.putExtra("_id", company.getId());
                startActivity(companyIntent);
                Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.enter, R.anim.exit);
            });

            mStationsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            mStationsRecyclerView.setAdapter(mAdapter);
        } else {
            cardStations.clear();
            cardStations.addAll(stations);
            mAdapter.createItems();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void onStationClick(Station station) {
        cardStations.clear();
        cardStations.add(station);
        mAdapter.createItems();
        mAdapter.notifyDataSetChanged();
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public boolean singleTapConfirmedHelper(GeoPoint p) {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        cardStations.clear();
        cardStations.addAll(stations);
        mAdapter.createItems();
        mAdapter.notifyDataSetChanged();
        return true;
    }

    @Override
    public boolean longPressHelper(GeoPoint p) {
        return false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable("location", myLocationOverlay.getLocation());
        savePrefs();
    }

    private void savePrefs() {
        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("WEINNAVIGATOR", MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putFloat("MAP_ZOOM_LEVEL_F", (float) map.getZoomLevelDouble());
        GeoPoint c = (GeoPoint) map.getMapCenter();
        ed.putFloat("MAP_CENTER_LAT", (float) c.getLatitude());
        ed.putFloat("MAP_CENTER_LON", (float) c.getLongitude());
        ed.apply();
    }

    private void setView() {
        if (stations.size() != 0) {
            double minLat = Double.MAX_VALUE;
            double maxLat = Double.MIN_VALUE;
            double minLong = Double.MAX_VALUE;
            double maxLong = Double.MIN_VALUE;
            ArrayList<GeoPoint> items = new ArrayList<>();
            int n = stations.size();
            for (int i = 0; i < n; i++) {
                Station station = stations.get(i);
                items.add(new GeoPoint(station.getLat(), station.getLon()));
            }
            for (GeoPoint item : items) {
                if (item.getLatitude() < minLat)
                    minLat = item.getLatitude();
                if (item.getLatitude() > maxLat)
                    maxLat = item.getLatitude();
                if (item.getLongitude() < minLong)
                    minLong = item.getLongitude();
                if (item.getLongitude() > maxLong)
                    maxLong = item.getLongitude();
            }
            BoundingBox boundingBox = new BoundingBox(maxLat, maxLong, minLat, minLong);
            map.zoomToBoundingBox(boundingBox, true);
        }
    }

    /**
     * Gets road async.
     *
     * @param stations the stations
     */
    public void getRoadAsync(List<Station> stations) {
        mRoad = null;
        Station startEndStation = stations.get(0);

        ArrayList<GeoPoint> wayPoints = new ArrayList<>(2);
        for (Station s : stations) {
            wayPoints.add(new GeoPoint(s.getLat(), s.getLon()));
        }
        wayPoints.add(new GeoPoint(startEndStation.getLat(), startEndStation.getLon()));
        new UpdateRoadTask().execute(wayPoints);
    }

    public void onResume() {
        super.onResume();
        boolean isOneProviderEnabled = startLocationUpdates();
        myLocationOverlay.setEnabled(isOneProviderEnabled);
        map.onResume();
    }

    private void updateUIWithRoads(Road road) {
        List<Overlay> mapOverlays = map.getOverlays();
        if (mRoadOverlays != null) {
            mapOverlays.remove(mRoadOverlays);
            mRoadOverlays = null;
        }
        if (road == null)
            return;
        if (road.mStatus == Road.STATUS_TECHNICAL_ISSUE)
            Toast.makeText(map.getContext(), "Technical issue when getting the route", Toast.LENGTH_SHORT).show();
        else if (road.mStatus > Road.STATUS_TECHNICAL_ISSUE) //functional issues
            Toast.makeText(map.getContext(), "No possible route here", Toast.LENGTH_SHORT).show();
        mRoadOverlays = new Polyline();

        Polyline roadPolyline = RoadManager.buildRoadOverlay(road);
        mRoadOverlays = roadPolyline;
        String routeDesc = road.getLengthDurationText(Objects.requireNonNull(getContext()), -1);
        String[] routeDescArray = routeDesc.split(",");
        callback.onRouteInformation(routeDescArray[0], routeDescArray[1]);
        mapOverlays.add(1, roadPolyline);
        Paint p = mRoadOverlays.getPaint();
        p.setColor(getResources().getColor(R.color.colorPrimary, getContext().getTheme()));
        p.setStrokeWidth(8);
    }

    public void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.removeUpdates(this);
        }
        map.onPause();
    }

    @Override
    public void onLocationChanged(final Location pLoc) {
        long currentTime = System.currentTimeMillis();
        if (mIgnorer.shouldIgnore(pLoc.getProvider(), currentTime))
            return;
        double dT = currentTime - mLastTime;
        if (dT < 100.0) {
            return;
        }
        mLastTime = currentTime;

        GeoPoint newLocation = new GeoPoint(pLoc);
        if (!myLocationOverlay.isEnabled()) {
            myLocationOverlay.setEnabled(true);
            map.getController().animateTo(newLocation);
        }

        myLocationOverlay.setLocation(newLocation);
        myLocationOverlay.setAccuracy((int) pLoc.getAccuracy());

        map.invalidate();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @SuppressLint("StaticFieldLeak")
    private class UpdateRoadTask extends AsyncTask<ArrayList<GeoPoint>, Void, Road> {
        @SafeVarargs
        protected final Road doInBackground(ArrayList<GeoPoint>... params) {
            ArrayList<GeoPoint> wayPoints = params[0];
            RoadManager roadManager;

            roadManager = new MapQuestRoadManager(BuildConfig.MAPQUEST_API_KEY);
            roadManager.addRequestOption("routeType=PEDESTRIAN");
            return roadManager.getRoad(wayPoints);
        }

        protected void onPostExecute(Road result) {
            mRoad = result;
            updateUIWithRoads(result);
        }
    }
}
