package de.vonhaarenfelix.weinnavigator.interfaces;


import de.vonhaarenfelix.weinnavigator.models.Weather;

/**
 * The interface On weather update.
 */
public interface OnWeatherUpdate {
    /**
     * Update.
     *
     * @param weather the weather
     */
    void update(Weather.WeatherResult weather);
}
