package de.vonhaarenfelix.weinnavigator.interfaces;

import de.vonhaarenfelix.weinnavigator.models.Event;

/**
 * The interface Event item click listener.
 */
public interface EventItemClickListener {
    /**
     * On item click.
     *
     * @param event the event
     */
    void onItemClick(Event event);
}
