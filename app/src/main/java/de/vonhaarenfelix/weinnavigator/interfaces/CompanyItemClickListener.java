package de.vonhaarenfelix.weinnavigator.interfaces;

import de.vonhaarenfelix.weinnavigator.models.Company;

/**
 * The interface Company item click listener.
 */
public interface CompanyItemClickListener {
    /**
     * On item click.
     *
     * @param company the company
     */
    void onItemClick(Company company);
}
