package de.vonhaarenfelix.weinnavigator.interfaces;

/**
 * The interface Fragment communication.
 */
public interface FragmentCommunication {
    /**
     * On route information.
     *
     * @param routeKm  the route km
     * @param routeMin the route min
     */
    void onRouteInformation(String routeKm, String routeMin);
}