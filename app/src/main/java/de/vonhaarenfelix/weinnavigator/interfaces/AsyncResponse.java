package de.vonhaarenfelix.weinnavigator.interfaces;

import org.json.JSONObject;

/**
 * The interface Async response.
 */
public interface AsyncResponse {
    /**
     * Process finish.
     *
     * @param output the output
     */
    void processFinish(JSONObject output);
}

