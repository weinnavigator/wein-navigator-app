package de.vonhaarenfelix.weinnavigator.interfaces;

import android.location.Location;

/**
 * The interface Location update.
 */
public interface LocationUpdate {
    /**
     * On location update.
     *
     * @param location the location
     */
    void OnLocationUpdate(Location location);
}
