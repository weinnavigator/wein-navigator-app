package de.vonhaarenfelix.weinnavigator.adapters;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.interfaces.CompanyItemClickListener;
import de.vonhaarenfelix.weinnavigator.models.Company;
import de.vonhaarenfelix.weinnavigator.models.Station;
import de.vonhaarenfelix.weinnavigator.models.listModels.ListItem;
import de.vonhaarenfelix.weinnavigator.models.listModels.StationHeaderItem;
import de.vonhaarenfelix.weinnavigator.models.listModels.StationItem;

/**
 * The type Station adapter.
 */
public class StationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final CompanyItemClickListener listener;
    private final ArrayList<Station> stations;
    private final List<ListItem> mItems = new ArrayList<>();

    /**
     * Instantiates a new Station adapter.
     *
     * @param stations the stations
     * @param listener the listener
     */
    public StationAdapter(ArrayList<Station> stations, CompanyItemClickListener listener) {
        this.stations = stations;
        this.listener = listener;
        createItems();
    }

    /**
     * Create items.
     */
    public void createItems() {
        mItems.clear();
        for (int i = 0; i < stations.size(); i++) {
            StationHeaderItem header = new StationHeaderItem();
            header.setNumber(stations.get(i).getSortIndex() + 1);
            header.setToilet(stations.get(i).getToilet());
            mItems.add(header);
            for (Company company : stations.get(i).getCompanies()) {
                StationItem item = new StationItem();
                item.setCompany(company);
                mItems.add(item);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                View itemView = inflater.inflate(R.layout.card_view_station_header, parent, false);
                return new HeaderViewHolder(itemView);
            }
            case ListItem.TYPE_BODY: {
                View itemView = inflater.inflate(R.layout.card_view_station_item, parent, false);
                return new StationViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                StationHeaderItem header = (StationHeaderItem) mItems.get(position);
                HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
                holder.stationName.setText(header.getName());
                if (header.isToilet()) {
                    holder.toiletIcon.setVisibility(View.VISIBLE);
                }
                break;
            }
            case ListItem.TYPE_BODY: {
                StationItem stationItem = (StationItem) mItems.get(position);
                StationViewHolder holder = (StationViewHolder) viewHolder;
                holder.stationName.setText(stationItem.getCompany().getName());
                holder.stationType.setText(stationItem.getCompany().getCategory());
                setImage(holder.stationLogo, stationItem.getCompany().getLogo());
                holder.setOnItemClickListener((View v) -> listener.onItemClick(stationItem.getCompany()));
                break;
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @SuppressWarnings("")
    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    private static class HeaderViewHolder extends RecyclerView.ViewHolder {

        /**
         * The Station name.
         */
        final TextView stationName;
        /**
         * The Toilet icon.
         */
        final ImageView toiletIcon;

        /**
         * Instantiates a new Header view holder.
         *
         * @param itemView the item view
         */
        HeaderViewHolder(View itemView) {
            super(itemView);
            stationName = itemView.findViewById(R.id.station_name);
            toiletIcon = itemView.findViewById(R.id.toilet_icon);
        }

    }

    private static class StationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View.OnClickListener mListener;
        /**
         * The Station type.
         */
        final TextView stationType;
        /**
         * The Station name.
         */
        final TextView stationName;
        /**
         * The Station logo.
         */
        final ImageView stationLogo;

        /**
         * Instantiates a new Station view holder.
         *
         * @param itemView the item view
         */
        StationViewHolder(View itemView) {
            super(itemView);
            stationType = itemView.findViewById(R.id.station_type);
            stationName = itemView.findViewById(R.id.station_name);
            stationLogo = itemView.findViewById(R.id.station_logo);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onClick(v);
            }
        }

        private void setOnItemClickListener(View.OnClickListener listener) {
            mListener = listener;
        }
    }

    private void setImage(ImageView view, String url) {
        Picasso.get()
                .load(url)
                .transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        int targetWidth = view.getWidth();
                        double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                        int targetHeight = (int) (targetWidth * aspectRatio);
                        if (targetHeight <= 0 || targetWidth <= 0) return source;
                        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                        if (result != source) {
                            // Same bitmap is returned if sizes are the same
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "key";
                    }
                })
                .into(view);
    }
}
