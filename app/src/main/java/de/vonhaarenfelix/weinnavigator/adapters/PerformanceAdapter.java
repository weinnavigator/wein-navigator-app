package de.vonhaarenfelix.weinnavigator.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.models.Performance;
import de.vonhaarenfelix.weinnavigator.models.listModels.HeaderItem;
import de.vonhaarenfelix.weinnavigator.models.listModels.ListItem;
import de.vonhaarenfelix.weinnavigator.models.listModels.PerformanceItem;

/**
 * The type Performance adapter.
 */
public class PerformanceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<Performance> performances;
    private final Map<String, ArrayList<Performance>> sortedPerformances = new ArrayMap<>();
    private final List<ListItem> mItems = new ArrayList<>();

    /**
     * Instantiates a new Performance adapter.
     *
     * @param performances the performances
     */
    public PerformanceAdapter(ArrayList<Performance> performances) {
        this.performances = performances;
        createItems();
    }

    private void createItems() {
        mItems.clear();
        sortedPerformances.clear();
        for (Performance performance : performances) {
            if (sortedPerformances.get(performance.getStationName()) == null) {
                ArrayList<Performance> performances = new ArrayList<>();
                performances.add(performance);
                sortedPerformances.put(performance.getStationName(), performances);
            } else {
                ArrayList<Performance> performances = sortedPerformances.get(performance.getStationName());
                Objects.requireNonNull(performances).add(performance);
                sortedPerformances.put(performance.getStationName(), performances);
            }
        }

        for (String stationName : sortedPerformances.keySet()) {
            HeaderItem header = new HeaderItem();
            header.setHeadline(stationName);
            mItems.add(header);
            for (Performance performance : Objects.requireNonNull(sortedPerformances.get(stationName))) {
                PerformanceItem item = new PerformanceItem();
                item.setPerformance(performance);
                mItems.add(item);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                View itemView = inflater.inflate(R.layout.sorted_list_header_item, parent, false);
                return new HeaderViewHolder(itemView);
            }
            case ListItem.TYPE_BODY: {
                View itemView = inflater.inflate(R.layout.left_right_list_item, parent, false);
                return new PerformanceViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                HeaderItem header = (HeaderItem) mItems.get(position);
                HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
                holder.headline.setText(header.getHeadline());
                break;
            }
            case ListItem.TYPE_BODY: {
                PerformanceItem perItem = (PerformanceItem) mItems.get(position);
                Performance performance = perItem.getPerformance();
                PerformanceViewHolder holder = (PerformanceViewHolder) viewHolder;
                holder.leftText.setText(performance.getEventDate().getDates(" - "));
                holder.rightText.setText(performance.getEventDate().getTimes(" - "));
                break;
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    private static class HeaderViewHolder extends RecyclerView.ViewHolder {

        /**
         * The Headline.
         */
        final TextView headline;

        /**
         * Instantiates a new Header view holder.
         *
         * @param itemView the item view
         */
        HeaderViewHolder(View itemView) {
            super(itemView);
            headline = itemView.findViewById(R.id.headline);
        }
    }

    private static class PerformanceViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Left text.
         */
        final TextView leftText;
        /**
         * The Right text.
         */
        final TextView rightText;

        /**
         * Instantiates a new Performance view holder.
         *
         * @param itemView the item view
         */
        PerformanceViewHolder(View itemView) {
            super(itemView);
            leftText = itemView.findViewById(R.id.left_text);
            rightText = itemView.findViewById(R.id.right_text);
        }
    }
}
