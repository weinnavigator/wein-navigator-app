package de.vonhaarenfelix.weinnavigator.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import de.vonhaarenfelix.weinnavigator.fragments.FragmentBandPerformance;
import de.vonhaarenfelix.weinnavigator.fragments.FragmentCompanyInfo;

/**
 * The type Band pager adapter.
 */
public class BandPagerAdapter extends FragmentStatePagerAdapter {
    /**
     * Instantiates a new Band pager adapter.
     *
     * @param fm the fm
     */
    public BandPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentBandPerformance();
            case 1:
                return new FragmentCompanyInfo();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Auftritte";
            case 1:
                return "Info";
            default:
                return null;
        }
    }
}

