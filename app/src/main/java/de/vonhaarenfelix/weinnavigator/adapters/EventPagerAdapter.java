package de.vonhaarenfelix.weinnavigator.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * The type Event pager adapter.
 */
public class EventPagerAdapter extends FragmentStatePagerAdapter {
    private final ArrayList<Fragment> list;

    /**
     * Instantiates a new Event pager adapter.
     *
     * @param fm   the fm
     * @param list the list
     */
    public EventPagerAdapter(FragmentManager fm, ArrayList<Fragment> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Info";
            case 1:
                return "Stationen";
            default:
                return null;
        }
    }
}
