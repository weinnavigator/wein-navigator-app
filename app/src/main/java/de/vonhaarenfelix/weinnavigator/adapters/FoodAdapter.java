package de.vonhaarenfelix.weinnavigator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.models.Food;

/**
 * The type Food adapter.
 */
public class FoodAdapter extends BaseAdapter {

    private final Context context;
    private final List<Food> foodList;

    /**
     * Instantiates a new Food adapter.
     *
     * @param context the context
     * @param persons the persons
     */
    public FoodAdapter(Context context, List<Food> persons) {
        this.context = context;
        this.foodList = persons;
    }

    @Override
    public int getCount() {
        return foodList.size();
    }

    @Override
    public Object getItem(int position) {
        return foodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.left_right_list_item, parent, false);
        }

        TextView name = convertView.findViewById(R.id.left_text);
        TextView price = convertView.findViewById(R.id.right_text);

        name.setText(foodList.get(position).getName());
        String foodPrice = String.format(Locale.getDefault(), "%.2f €", foodList.get(position).getNumber());
        price.setText(foodPrice);

        return convertView;
    }
}