package de.vonhaarenfelix.weinnavigator.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.interfaces.EventItemClickListener;
import de.vonhaarenfelix.weinnavigator.models.Event;

/**
 * The type Events adapter.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {
    private final EventItemClickListener listener;
    private final ArrayList<Event> events;


    /**
     * Instantiates a new Events adapter.
     *
     * @param events   the events
     * @param listener the listener
     */
    public EventsAdapter(ArrayList<Event> events, EventItemClickListener listener) {
        this.listener = listener;
        this.events = events;
    }

    @NonNull
    @Override
    public EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View inflatedView = inflater.inflate(R.layout.event_list_item, parent, false);
        return new EventsViewHolder(inflatedView);
    }


    @Override
    public void onBindViewHolder(@NonNull EventsViewHolder holder, int position) {
        Event event = events.get(position);

        holder.mEventTitleTextView.setText(event.getName());
        holder.mEventCategoryTextView.setText(event.getCategory());
        holder.setImage(event.getPreviewUrl());
        holder.mEventDateTextView.setText(event.getEventDate().getDates(" - "));
        holder.mEventTimeTextView.setText(event.getEventDate().getTimes(" - "));
        holder.mEventLocationTextView.setText(event.getEventLocation().getCity());
        if (event.getEventLocation().getDistance() != null) {
            holder.mEventDistanceTextView.setText(event.getEventLocation().getDistance());
        }

        holder.setOnItemClickListener((View v) -> listener.onItemClick(events.get(position)));
    }

    @Override
    public int getItemCount() {
        if (events == null) {
            return 0;
        } else {
            return events.size();
        }
    }

    /**
     * The type Events view holder.
     */
    class EventsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View.OnClickListener mListener;
        /**
         * The M event title text view.
         */
        final TextView mEventTitleTextView;
        /**
         * The M event category text view.
         */
        final TextView mEventCategoryTextView;
        /**
         * The M event image view.
         */
        final ImageView mEventImageView;
        /**
         * The M event date text view.
         */
        final TextView mEventDateTextView;
        /**
         * The M event time text view.
         */
        final TextView mEventTimeTextView;
        /**
         * The M event location text view.
         */
        final TextView mEventLocationTextView;
        /**
         * The M event distance text view.
         */
        final TextView mEventDistanceTextView;

        /**
         * Instantiates a new Events view holder.
         *
         * @param parentView the parent view
         */
        EventsViewHolder(View parentView) {
            super(parentView);

            mEventTitleTextView = parentView.findViewById(R.id.eventTitle);
            mEventCategoryTextView = parentView.findViewById(R.id.eventCategory);
            mEventImageView = parentView.findViewById(R.id.eventImage);
            mEventDateTextView = parentView.findViewById(R.id.eventDate);
            mEventTimeTextView = parentView.findViewById(R.id.eventTime);
            mEventLocationTextView = parentView.findViewById(R.id.eventLocation);
            mEventDistanceTextView = parentView.findViewById(R.id.eventDistance);

            parentView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            // when view is clicked, simply forward the call
            if (mListener != null) {
                mListener.onClick(v);
            }
        }

        /**
         * Sets on item click listener.
         *
         * @param listener the listener
         */
        void setOnItemClickListener(View.OnClickListener listener) {
            mListener = listener;
        }

        private void setImage(String url) {
            Picasso.get()
                    .load(url)
                    .centerCrop()
                    .fit()
                    .into(mEventImageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e("Image", e.toString());
                        }
                    });
        }
    }
}