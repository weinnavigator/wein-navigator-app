package de.vonhaarenfelix.weinnavigator.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.vonhaarenfelix.weinnavigator.R;
import de.vonhaarenfelix.weinnavigator.models.Drink;
import de.vonhaarenfelix.weinnavigator.models.listModels.DrinkItem;
import de.vonhaarenfelix.weinnavigator.models.listModels.HeaderItem;
import de.vonhaarenfelix.weinnavigator.models.listModels.ListItem;

/**
 * The type Drink adapter.
 */
public class DrinkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<Drink> drinks;
    private final Map<String, ArrayList<Drink>> sortedDrinks = new ArrayMap<>();
    private final List<ListItem> mItems = new ArrayList<>();

    /**
     * Instantiates a new Drink adapter.
     *
     * @param drinks the drinks
     */
    public DrinkAdapter(ArrayList<Drink> drinks) {
        this.drinks = drinks;
        createItems();
    }

    private void createItems() {
        mItems.clear();
        sortedDrinks.clear();
        for (Drink drink : drinks) {
            if (sortedDrinks.get(drink.getCategory()) == null) {
                ArrayList<Drink> drinks = new ArrayList<>();
                drinks.add(drink);
                sortedDrinks.put(drink.getCategory(), drinks);
            } else {
                ArrayList<Drink> drinks = sortedDrinks.get(drink.getCategory());
                Objects.requireNonNull(drinks).add(drink);
                sortedDrinks.put(drink.getCategory(), drinks);
            }
        }

        for (String category : sortedDrinks.keySet()) {
            HeaderItem header = new HeaderItem();
            header.setHeadline(category);
            mItems.add(header);
            for (Drink drink : Objects.requireNonNull(sortedDrinks.get(category))) {
                DrinkItem item = new DrinkItem();
                item.setDrink(drink);
                mItems.add(item);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                View itemView = inflater.inflate(R.layout.sorted_list_header_item, parent, false);
                return new HeaderViewHolder(itemView);
            }
            case ListItem.TYPE_BODY: {
                View itemView = inflater.inflate(R.layout.winegrower_drink_list_item, parent, false);
                return new DrinkViewHolder(itemView);
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ListItem.TYPE_HEADER: {
                HeaderItem header = (HeaderItem) mItems.get(position);
                HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
                holder.wineCategoryTextView.setText(header.getHeadline());
                break;
            }
            case ListItem.TYPE_BODY: {
                DrinkItem drinkItem = (DrinkItem) mItems.get(position);
                Drink drink = drinkItem.getDrink();
                DrinkViewHolder holder = (DrinkViewHolder) viewHolder;
                holder.drinkName.setText(drink.getName());
                String desc = drink.getYear() + " - " + drink.getDesc() + " - " + drink.getAlcohol() + " %";
                holder.drinkDesc.setText(desc);
                StringBuilder volumes = new StringBuilder();
                for (int i = 0; i < drink.getPrices().size(); i++) {
                    if (i + 1 < drink.getPrices().size()) {
                        volumes.append(drink.getPrices().get(i).getVolume()).append(" l\n");
                    } else {
                        volumes.append(drink.getPrices().get(i).getVolume()).append(" l");
                    }
                }
                holder.drinkVolume.setText(volumes.toString());
                StringBuilder prices = new StringBuilder();
                for (int i = 0; i < drink.getPrices().size(); i++) {
                    if (i + 1 < drink.getPrices().size()) {
                        prices.append(String.format(Locale.getDefault(), "%.2f €\n", drink.getPrices().get(i).getPrice()));
                    } else {
                        prices.append(String.format(Locale.getDefault(), "%.2f €", drink.getPrices().get(i).getPrice()));
                    }
                }
                holder.drinkPrice.setText(prices.toString());
                break;
            }
            default:
                throw new IllegalStateException("unsupported item type");
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getType();
    }

    private static class HeaderViewHolder extends RecyclerView.ViewHolder {

        /**
         * The Wine category text view.
         */
        final TextView wineCategoryTextView;

        /**
         * Instantiates a new Header view holder.
         *
         * @param itemView the item view
         */
        HeaderViewHolder(View itemView) {
            super(itemView);
            wineCategoryTextView = itemView.findViewById(R.id.headline);
        }
    }

    private static class DrinkViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Drink name.
         */
        final TextView drinkName;
        /**
         * The Drink desc.
         */
        final TextView drinkDesc;
        /**
         * The Drink volume.
         */
        final TextView drinkVolume;
        /**
         * The Drink price.
         */
        final TextView drinkPrice;

        /**
         * Instantiates a new Drink view holder.
         *
         * @param itemView the item view
         */
        DrinkViewHolder(View itemView) {
            super(itemView);
            drinkName = itemView.findViewById(R.id.drink_name);
            drinkDesc = itemView.findViewById(R.id.drink_desc);
            drinkVolume = itemView.findViewById(R.id.drink_volume);
            drinkPrice = itemView.findViewById(R.id.drink_price);
        }
    }
}
