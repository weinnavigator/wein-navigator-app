package de.vonhaarenfelix.weinnavigator.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import de.vonhaarenfelix.weinnavigator.fragments.FragmentCompanyInfo;
import de.vonhaarenfelix.weinnavigator.fragments.FragmentWinegrowerDrinks;
import de.vonhaarenfelix.weinnavigator.fragments.FragmentWinegrowerFood;

/**
 * The type Winegrower pager adapter.
 */
public class WinegrowerPagerAdapter extends FragmentStatePagerAdapter {
    /**
     * Instantiates a new Winegrower pager adapter.
     *
     * @param fm the fm
     */
    public WinegrowerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentWinegrowerDrinks();
            case 1:
                return new FragmentWinegrowerFood();
            case 2:
                return new FragmentCompanyInfo();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Drinks";
            case 1:
                return "Food";
            case 2:
                return "Info";
            default:
                return null;
        }
    }
}

