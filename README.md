# Wein-Navigator

This application should provide a better overview for wine hikings.

## Servers

### Data Server (Heroku)
Base URL: https://wein-navigator.herokuapp.com/
Server is normally in sleep state, it will take some time to wake up.
---
REST API doc: https://documenter.getpostman.com/view/5749628/S1Lu2pX5

## APIs
- Weather API (OpenWeatherAPI) https://openweathermap.org/api - API key: ***
- Map API (OpenStreetMap) https://github.com/osmdroid/osmdroid - API key: ***

## Android Specs
- min. OS Version: 5.0
- Android Studio Version: 3.4

## Design
can be found in the linked Dropbox: https://www.dropbox.com/home/Wein-Navigator (shared access required)
